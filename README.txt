
-- SUMMARY --

Provides an option to order the images styles in a specific order drag and drop.

-- REQUIREMENTS --

* None.

INSTALLATION
------------

1. Copy the imagestyles_sort directory to your sites/SITENAME/modules directory.

2. Enable the module at Administer >> Site building >> Modules.
